#!/bin/sh

export OUTPUT="$2"
export CARGO_TARGET_DIR="$3"/target
export CARGO_HOME="$CARGO_TARGET_DIR"/cargo-home
# export STYLISH_PROFILE="$4"

CARGO_TOML=$1/subprojects/stylish/Cargo.toml

if [ "$STYLISH_PROFILE" = "Devel" ]
then
    echo "DEBUG MODE"
    cargo build --manifest-path $CARGO_TOML && cp "$CARGO_TARGET_DIR"/debug/libstylish.a "$OUTPUT"
else
    echo "RELEASE MODE"
    cargo build --manifest-path $CARGO_TOML --release && cp "$CARGO_TARGET_DIR"/release/libstylish.a "$OUTPUT"
fi
