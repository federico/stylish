use crate::length::NormalizeParams;

/// Trait for types that can be constructed by normalizing `Length` values from other types.
///
/// Many low-level types contain `Length` values; for example, a `Shadow` has `xoffset`
/// and `yoffset` fields of type `Length`, and those lengths have units (cm, pixels,
/// etc.).  Gnome-shell's drawing code wants to have lengths normalized to pixels.  Types
/// that implement `Normalize` can normalize themselves from lengths with units into just
/// pixels.
pub trait Normalize {
    type Out;

    fn normalize(&self, norm: &NormalizeParams) -> Self::Out;
}

/// Similar to `Normalize`, but the result is adjusted to the pixel grid.
pub trait NormalizeToPixelGrid {
    type Out;

    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> Self::Out;
}
