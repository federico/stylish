use cssparser::{self, Parser, Token};

use crate::error::ParseError;
use crate::parsers::Parse;

#[derive(Copy, Clone, Debug, Default, PartialEq)]
#[repr(C)]
pub struct Time {
    pub millis: f64,
}

impl Time {
    fn new(millis: f64) -> Time {
        Time { millis }
    }
}

impl Parse for Time {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Time, ParseError<'i>> {
        let state = parser.state();

        match parser.next()? {
            Token::Dimension { value, unit, .. } if *unit == "s" => {
                Ok(Time::new(f64::from(*value) * 1000.0))
            }

            Token::Dimension { value, unit, .. } if *unit == "ms" => {
                Ok(Time::new(f64::from(*value)))
            }

            _ => {
                parser.reset(&state);
                Err(parser.new_error_for_next_token())
            }
        }
    }
}

impl From<Time> for f64 {
    fn from(t: Time) -> f64 {
        t.millis
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_time() {
        assert_eq!(Time::parse_str("5s"), Ok(Time::new(5000.0)));
        assert_eq!(Time::parse_str("5ms"), Ok(Time::new(5.0)));
    }

    #[test]
    fn invalid_time_yields_error() {
        assert!(Time::parse_str("").is_err());
        assert!(Time::parse_str("5").is_err());
        assert!(Time::parse_str("3 s").is_err());
        assert!(Time::parse_str("3musketeers").is_err());
    }
}
