use cssparser::{Parser, Token};

use crate::background::BackgroundSize;
use crate::error::*;
use crate::font_props::{Font, FontFamily, FontSize, FontWeight, LineHeight};
use crate::length::LengthOrAuto;
use crate::parsers::Parse;
use crate::property_macros::Property;

// Dummy placeholder until we can use librsvg's infrastructure
pub type ComputedValues = ();

// https://drafts.csswg.org/css-backgrounds-3/#background-size
make_property!(
    ComputedValues,
    BackgroundSize,
    default: BackgroundSize::Explicit {
        width: LengthOrAuto::Auto,
        height: LengthOrAuto::Auto,
    },
    inherits_automatically: false,
);

// https://drafts.csswg.org/css-fonts-4/#font-prop
make_property!(
    ComputedValues,
    Font,
    default: Font::Spec(Default::default()),
    inherits_automatically: true,
);

// https://www.w3.org/TR/SVG/text.html#FontFamilyProperty
make_property!(
    ComputedValues,
    FontFamily,
    default: FontFamily("sans".to_string()),
    inherits_automatically: true,
);

// https://www.w3.org/TR/SVG/text.html#FontSizeProperty
make_property!(
    ComputedValues,
    FontSize,
    // FIXME: this deviates from librsvg, where the default is FontSizeSpec::Value(Length::parse_str("12.0").unwrap()),
    default: FontSize::Medium,
    property_impl: {
        impl Property<ComputedValues> for FontSize {
            fn inherits_automatically() -> bool {
                true
            }

            fn compute(&self, _v: &ComputedValues) -> Self {
                // We don't have ComputedValues yet; that will come from librsvg later
                unreachable!()
            }
        }
    }
);

// https://www.w3.org/TR/SVG/text.html#FontStretchProperty
make_property!(
    ComputedValues,
    FontStretch,
    default: Normal,
    inherits_automatically: true,

    identifiers:
    "normal" => Normal,
    "wider" => Wider,
    "narrower" => Narrower,
    "ultra-condensed" => UltraCondensed,
    "extra-condensed" => ExtraCondensed,
    "condensed" => Condensed,
    "semi-condensed" => SemiCondensed,
    "semi-expanded" => SemiExpanded,
    "expanded" => Expanded,
    "extra-expanded" => ExtraExpanded,
    "ultra-expanded" => UltraExpanded,
);

// https://www.w3.org/TR/SVG/text.html#FontStyleProperty
make_property!(
    ComputedValues,
    FontStyle,
    default: Normal,
    inherits_automatically: true,

    identifiers:
    "normal" => Normal,
    "italic" => Italic,
    "oblique" => Oblique,
);

// https://drafts.csswg.org/css-fonts-4/#font-weight-prop
make_property!(
    ComputedValues,
    FontWeight,
    default: FontWeight::Normal,
    property_impl: {
        impl Property<ComputedValues> for FontWeight {
            fn inherits_automatically() -> bool {
                true
            }

            fn compute(&self, _v: &ComputedValues) -> Self {
                // We don't have ComputedValues yet; that will come from librsvg later
                unreachable!()
            }
        }
    }
);

// https://www.w3.org/TR/SVG/text.html#FontVariantProperty
make_property!(
    ComputedValues,
    FontVariant,
    default: Normal,
    inherits_automatically: true,

    identifiers:
    "normal" => Normal,
    "small-caps" => SmallCaps,
);

// keep order in sync with st-theme-node.h:StGradientType
make_property!(
    ComputedValues,
    StGradientType,
    default: None,
    inherits_automatically: false,

    identifiers:
    "none" => None,
    "vertical" => Vertical,
    "horizontal" => Horizontal,
    "radial" => Radial,
);

// https://drafts.csswg.org/css2/visudet.html#propdef-line-height
make_property!(
    ComputedValues,
    LineHeight,
    default: LineHeight::Normal,
    inherits_automatically: true,
);

// keep order in sync with st-theme-node.h:StIconStyle
make_property!(
    (),
    StIconStyle,
    default: Requested,
    inherits_automatically: false,
    identifiers:
    "requested" => Requested,
    "regular" => Regular,
    "symbolic" => Symbolic,
);

// keep order in sync with st-theme-node.h:StTextAlign
make_property!(
    (),
    StTextAlign,
    default: Left,
    inherits_automatically: true,
    identifiers:
    "left" => Left,
    "center" => Center,
    "right" => Right,
    "justify" => Justify,
);

make_property!(
    (),
    TextDecoration,
    inherits_automatically: false,

    fields: {
        overline: bool, default: false,
        underline: bool, default: false,
        line_through: bool, default: false,
        blink: bool, default: false,
    }

    parse_impl: {
        impl Parse for TextDecoration {
            fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<TextDecoration, ParseError<'i>> {
                let mut underline = false;
                let mut overline = false;
                let mut line_through = false;
                let mut blink = false;

                if parser.try_parse(|p| p.expect_ident_matching("none")).is_ok() {
                    return Ok(TextDecoration::default());
                }

                while !parser.is_exhausted() {
                    let loc = parser.current_source_location();
                    let token = parser.next()?;

                    match token {
                        Token::Ident(ref cow) if cow.eq_ignore_ascii_case("underline") => underline = true,
                        Token::Ident(ref cow) if cow.eq_ignore_ascii_case("overline") => overline = true,
                        Token::Ident(ref cow) if cow.eq_ignore_ascii_case("line-through") => line_through = true,
                        Token::Ident(ref cow) if cow.eq_ignore_ascii_case("blink") => blink = true,
                        _ => Err(loc.new_basic_unexpected_token_error(token.clone()))?,
                    }
                }

                Ok(TextDecoration {
                    overline,
                    underline,
                    line_through,
                    blink,
                })
            }
        }
    }
);
