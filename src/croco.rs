// This is a hand-written binding to a very minimal part of libcroco.  We don't use bindgen because
// it wants to import pretty much all of glib's types and functions, and we just need a few.

use libc;

// Opaque types from libcroco, or those which we only manipulate through libcroco functions
#[repr(C)]
pub struct CRTerm {
    _private: [u8; 0],
}

extern "C" {
    pub fn cr_term_to_string(a_this: *const CRTerm) -> *mut libc::c_char;
    pub fn cr_term_one_to_string(a_this: *const CRTerm) -> *mut libc::c_char;
}
