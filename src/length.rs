use cast::f64;
use cssparser::{self, Parser, Token};

use crate::error::ParseError;
use crate::normalize::{Normalize, NormalizeToPixelGrid};
use crate::parsers::Parse;

// Keep this in sync with st-theme-node.c:NormalizeParams
#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct NormalizeParams {
    pub font_size: f64,
    pub resolution: f64,
    pub scale_factor: i32,
}

/// Units for length values.
#[repr(C)]
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum LengthUnit {
    /// `1.0` means 100%
    Percent,

    /// Pixels, or the CSS default unit
    Px,

    /// Size of the current font
    Em,

    /// x-height of the current font
    Ex,

    /// Inches (25.4 mm)
    In,

    /// Centimeters
    Cm,

    /// Millimeters
    Mm,

    /// Points (1/72 inch)
    Pt,

    /// Picas (12 points)
    Pc,
}

/// A CSS length value.
///
/// This is equivalent to [CSS lengths].
///
/// [CSS lengths]: https://www.w3.org/TR/CSS22/syndata.html#length-units
///
/// It is up to the calling application to convert lengths in non-pixel units
/// (i.e. those where the [`unit`] field is not [`LengthUnit::Px`]) into something
/// meaningful to the application.  For example, if your application knows the
/// dots-per-inch (DPI) it is using, it can convert lengths with [`unit`] in
/// [`LengthUnit::In`] or other physical units.
///
/// [`unit`]: #structfield.unit
/// [`LengthUnit::Px`]: enum.LengthUnit.html#variant.Px
/// [`LengthUnit::In`]: enum.LengthUnit.html#variant.In
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Length {
    /// Numeric part of the length
    pub length: f64,

    /// Unit part of the length
    pub unit: LengthUnit,
}

impl Default for Length {
    fn default() -> Length {
        Length::new(0.0, LengthUnit::Px)
    }
}

impl Parse for Length {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Length, ParseError<'i>> {
        let state = parser.state();

        let token = parser.next()?;

        match *token {
            Token::Number { value, .. } => Ok(Length::new(f64::from(value), LengthUnit::Px)),

            Token::Percentage { unit_value, .. } => {
                Ok(Length::new(f64::from(unit_value), LengthUnit::Percent))
            }

            Token::Dimension {
                value, ref unit, ..
            } => {
                let value = f64::from(value);

                match unit.as_ref() {
                    "px" => Ok(Length::new(value, LengthUnit::Px)),
                    "em" => Ok(Length::new(value, LengthUnit::Em)),
                    "ex" => Ok(Length::new(value, LengthUnit::Ex)),
                    "in" => Ok(Length::new(value, LengthUnit::In)),
                    "cm" => Ok(Length::new(value, LengthUnit::Cm)),
                    "mm" => Ok(Length::new(value, LengthUnit::Mm)),
                    "pt" => Ok(Length::new(value, LengthUnit::Pt)),
                    "pc" => Ok(Length::new(value, LengthUnit::Pc)),

                    _ => {
                        parser.reset(&state);
                        Err(parser.new_error_for_next_token())
                    }
                }
            }

            _ => {
                parser.reset(&state);
                Err(parser.new_error_for_next_token())
            }
        }
    }
}

pub const POINTS_PER_INCH: f64 = 72.0;
const CM_PER_INCH: f64 = 2.54;
const MM_PER_INCH: f64 = 25.4;
const PICA_PER_INCH: f64 = 6.0;

impl Length {
    pub fn new(length: f64, unit: LengthUnit) -> Length {
        Length { length, unit }
    }
}

impl Normalize for Length {
    type Out = f64;

    fn normalize(&self, norm: &NormalizeParams) -> f64 {
        match self.unit {
            LengthUnit::Px => self.length * f64::from(norm.scale_factor),

            LengthUnit::In => self.length * norm.resolution,
            LengthUnit::Cm => self.length * norm.resolution / CM_PER_INCH,
            LengthUnit::Mm => self.length * norm.resolution / MM_PER_INCH,
            LengthUnit::Pt => self.length * norm.resolution / POINTS_PER_INCH,
            LengthUnit::Pc => self.length * norm.resolution / PICA_PER_INCH,

            LengthUnit::Em => self.length * norm.font_size,

            // Doing better would require actually resolving the font description
            // to a specific font, and Pango doesn't have an ex metric anyways,
            // so we'd have to try and synthesize it by complicated means.
            //
            // The 0.5em is the CSS spec suggested thing to use when nothing
            // better is available.
            LengthUnit::Ex => self.length * norm.font_size * 0.5,

            LengthUnit::Percent => panic!("percent lengths can't be normalized"),
        }
    }
}

impl NormalizeToPixelGrid for Length {
    type Out = i32;

    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> i32 {
        // See https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/470
        // for the rationale behind this.

        let value = self.normalize(norm);

        // This should probably use round(), but the original code just does (int) (x + 0.5)
        ((value / f64(norm.scale_factor) + 0.5) as i32) * norm.scale_factor
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct NonPercentLength(pub Length);

impl Parse for NonPercentLength {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<NonPercentLength, ParseError<'i>> {
        let state = parser.state();

        Length::parse(parser).and_then(|l| {
            if l.unit == LengthUnit::Percent {
                parser.reset(&state);
                Err(parser.new_error_for_next_token())
            } else {
                Ok(NonPercentLength(l))
            }
        })
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LengthOrAuto {
    Length(Length),
    Auto,
}

impl Parse for LengthOrAuto {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<LengthOrAuto, ParseError<'i>> {
        if parser.try_parse(|p| p.expect_ident_matching("auto")).is_ok() {
            Ok(LengthOrAuto::Auto)
        } else {
            Ok(LengthOrAuto::Length(Length::parse(parser)?))
        }
    }
}

impl NormalizeToPixelGrid for LengthOrAuto {
    type Out = i32;

    // The -1 convention comes from from StBackgroundSize
    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> i32 {
        match *self {
            LengthOrAuto::Length(l) => l.normalize_to_pixel_grid(norm),
            LengthOrAuto::Auto => -1,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_length() {
        assert_eq!(Length::parse_str("5"), Ok(Length::new(5.0, LengthUnit::Px)));
        assert_eq!(
            Length::parse_str("5px"),
            Ok(Length::new(5.0, LengthUnit::Px))
        );
        assert_eq!(
            Length::parse_str("50%"),
            Ok(Length::new(0.5, LengthUnit::Percent))
        );
        assert_eq!(
            Length::parse_str("-5in"),
            Ok(Length::new(-5.0, LengthUnit::In))
        );
        assert_eq!(
            Length::parse_str("3.5cm"),
            Ok(Length::new(3.5, LengthUnit::Cm))
        );
        assert_eq!(
            Length::parse_str("3.5mm"),
            Ok(Length::new(3.5, LengthUnit::Mm))
        );
        assert_eq!(
            Length::parse_str("4.0pt"),
            Ok(Length::new(4.0, LengthUnit::Pt))
        );
        assert_eq!(
            Length::parse_str("4.0pc"),
            Ok(Length::new(4.0, LengthUnit::Pc))
        );
        assert_eq!(
            Length::parse_str("4.0em"),
            Ok(Length::new(4.0, LengthUnit::Em))
        );
        assert_eq!(
            Length::parse_str("4.0ex"),
            Ok(Length::new(4.0, LengthUnit::Ex))
        );
    }

    #[test]
    fn invalid_length_yields_error() {
        assert!(Length::parse_str("").is_err());
        assert!(Length::parse_str("2nostrils").is_err());
    }

    #[test]
    fn normalizes_px() {
        let norm = NormalizeParams {
            font_size: 1.0,
            resolution: 1.0,
            scale_factor: 4,
        };

        assert_eq!(Length::new(4.0, LengthUnit::Px).normalize(&norm), 16.0);
    }

    #[test]
    fn normalizes_absolute_units() {
        let norm = NormalizeParams {
            font_size: 10.0,
            resolution: 100.0,
            scale_factor: 4,
        };

        assert_eq!(Length::new(1.0, LengthUnit::In).normalize(&norm), 100.0);
        assert_eq!(Length::new(254.0, LengthUnit::Cm).normalize(&norm), 10000.0);
        assert_eq!(Length::new(254.0, LengthUnit::Mm).normalize(&norm), 1000.0);
        assert_eq!(Length::new(72.0, LengthUnit::Pt).normalize(&norm), 100.0);
        assert_eq!(Length::new(6.0, LengthUnit::Pc).normalize(&norm), 100.0);
    }

    #[test]
    fn normalizes_font_relative_units() {
        let norm = NormalizeParams {
            font_size: 72.0,
            resolution: 100.0,
            scale_factor: 4,
        };

        assert_eq!(Length::new(1.0, LengthUnit::Em).normalize(&norm), 72.0);
        assert_eq!(Length::new(1.0, LengthUnit::Ex).normalize(&norm), 36.0);
    }

    #[should_panic]
    #[test]
    fn cannot_normalize_percent() {
        let norm = NormalizeParams {
            font_size: 72.0,
            resolution: 100.0,
            scale_factor: 4,
        };

        assert_eq!(
            Length::new(100.0, LengthUnit::Percent).normalize(&norm),
            1.0
        );
    }

    #[test]
    fn non_percent_parses() {
        assert_eq!(
            NonPercentLength::parse_str("5"),
            Ok(NonPercentLength(Length::new(5.0, LengthUnit::Px)))
        );
    }

    #[test]
    fn non_percent_detects_error() {
        assert!(NonPercentLength::parse_str("5%").is_err());
    }

    #[test]
    fn normalizes_to_pixel_grid() {
        let norm = NormalizeParams {
            font_size: 1.0,
            resolution: 1.0,
            scale_factor: 2,
        };

        assert_eq!(Length::new(1.6, LengthUnit::Px).normalize_to_pixel_grid(&norm), 4);
    }
}
