use cssparser::{self, Parser};

use crate::error::ParseError;
use crate::length::{Length, NormalizeParams};
use crate::normalize::NormalizeToPixelGrid;
use crate::parsers::Parse;

// Keep this in sync with st-theme-node-private.h:StCorners
#[derive(Default)]
#[repr(C)]
pub struct StCorners {
    top_left: i32,
    top_right: i32,
    bottom_right: i32,
    bottom_left: i32,
}

#[derive(Default, Copy, Clone, PartialEq, Debug)]
pub struct Corners {
    top_left: Length,
    top_right: Length,
    bottom_right: Length,
    bottom_left: Length,
}

impl Parse for Corners {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Corners, ParseError<'i>> {
        let mut l = vec![];

        for _ in 0..4 {
            l.push(Length::parse(parser)?);

            if parser.is_exhausted() {
                break;
            }
        }

        #[rustfmt::skip]
        let corners = match l.len() {
            1 => Corners {
                top_left:     l[0],
                top_right:    l[0],
                bottom_right: l[0],
                bottom_left:  l[0],
            },

            2 => Corners {
                top_left:     l[0],
                bottom_right: l[0],
                top_right:    l[1],
                bottom_left:  l[1],
            },

            3 => Corners {
                top_left:     l[0],
                top_right:    l[1],
                bottom_left:  l[1],
                bottom_right: l[2],
            },

            4 => Corners {
                top_left:     l[0],
                top_right:    l[1],
                bottom_right: l[2],
                bottom_left:  l[3],
            },

            _ => unreachable!(),
        };

        Ok(corners)
    }
}

impl NormalizeToPixelGrid for Corners {
    type Out = StCorners;

    fn normalize_to_pixel_grid(self: &Corners, norm: &NormalizeParams) -> StCorners {
        let Corners {
            top_left,
            top_right,
            bottom_right,
            bottom_left,
        } = *self;

        StCorners {
            top_left: top_left.normalize_to_pixel_grid(norm),
            top_right: top_right.normalize_to_pixel_grid(norm),
            bottom_right: bottom_right.normalize_to_pixel_grid(norm),
            bottom_left: bottom_left.normalize_to_pixel_grid(norm),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::length::LengthUnit;

    #[rustfmt::skip]
    #[test]
    fn parses_corners() {
        assert_eq!(
            Corners::parse_str("1px"),
            Ok(Corners {
                top_left:     Length::new(1.0, LengthUnit::Px),
                top_right:    Length::new(1.0, LengthUnit::Px),
                bottom_right: Length::new(1.0, LengthUnit::Px),
                bottom_left:  Length::new(1.0, LengthUnit::Px),
            }),
        );

        assert_eq!(
            Corners::parse_str("1px 2px"),
            Ok(Corners {
                top_left:     Length::new(1.0, LengthUnit::Px),
                bottom_right: Length::new(1.0, LengthUnit::Px),
                top_right:    Length::new(2.0, LengthUnit::Px),
                bottom_left:  Length::new(2.0, LengthUnit::Px),
            }),
        );

        assert_eq!(
            Corners::parse_str("1px 2px 3px"),
            Ok(Corners {
                top_left:     Length::new(1.0, LengthUnit::Px),
                top_right:    Length::new(2.0, LengthUnit::Px),
                bottom_left:  Length::new(2.0, LengthUnit::Px),
                bottom_right: Length::new(3.0, LengthUnit::Px),
            }),
        );

        assert_eq!(
            Corners::parse_str("1px 2px 3px 4px"),
            Ok(Corners {
                top_left:     Length::new(1.0, LengthUnit::Px),
                top_right:    Length::new(2.0, LengthUnit::Px),
                bottom_right: Length::new(3.0, LengthUnit::Px),
                bottom_left:  Length::new(4.0, LengthUnit::Px),
            }),
        );
    }
}
