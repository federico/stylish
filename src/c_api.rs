use cssparser::{Parser, ParserInput};
use glib::translate::*;
use libc;
use pango;
use pango_sys;

use crate::background::{BackgroundPosition, BackgroundSize, StBackgroundPosition, StBackgroundSizeSpec};
use crate::color::{ClutterColor, Color};
use crate::corners::{Corners, StCorners};
use crate::croco::*;
use crate::error::*;
use crate::font_props::{Font, FontFamily, FontSize, FontWeight, StFontSpec, StTextDecoration};
use crate::length::{Length, NormalizeParams};
use crate::normalize::{Normalize, NormalizeToPixelGrid};
use crate::number::Number;
use crate::outline::{Outline, StOutline};
use crate::parsers::Parse;
use crate::properties::{parse_input, SpecifiedValue};
use crate::property_defs::{FontStyle, FontVariant, StGradientType, StIconStyle, StTextAlign, TextDecoration};
use crate::shadow::{Shadow, StParsedShadow};
use crate::sides::{Sides, StSides};
use crate::time::Time;
use crate::url::Url;

// Keep this in sync with st-theme-node.c:GetFromTermResult
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(C)]
pub enum GetFromTermResult {
    Found,
    NotFound,
    Inherit,
}

#[derive(Debug, PartialEq)]
enum ConvertedResult<T> {
    NotFound,
    Found(T),
    Inherit,
}

impl<'i, T> ConvertedResult<T> {
    fn from<S: Into<T> + Clone + Default>(r: Result<SpecifiedValue<S>, ParseError<'i>>) -> Self {
        match r {
            Ok(SpecifiedValue::Unspecified) => unreachable!(),
            Ok(SpecifiedValue::Specified(v)) => ConvertedResult::Found(v.into()),
            Ok(SpecifiedValue::Inherit) => ConvertedResult::Inherit,

            // We discard parse errors because gnome-shell does not have machinery to propagate them yet
            Err(_) => ConvertedResult::NotFound,
        }
    }

    fn map<V, F: FnOnce(T) -> V>(self, f: F) -> ConvertedResult<V> {
        match self {
            ConvertedResult::Found(v) => ConvertedResult::Found(f(v)),
            ConvertedResult::NotFound => ConvertedResult::NotFound,
            ConvertedResult::Inherit => ConvertedResult::Inherit,
        }
    }

    unsafe fn to_shell<O: From<T>>(self, out_val: *mut O) -> GetFromTermResult {
        match self {
            ConvertedResult::Found(v) => {
                *out_val = v.into();
                GetFromTermResult::Found
            }

            ConvertedResult::NotFound => GetFromTermResult::NotFound,

            ConvertedResult::Inherit => GetFromTermResult::Inherit,
        }
    }
}

// Whether a `*const CRTerm` represents a single term, or a term list
enum Terms {
    Single(*const CRTerm),
    List(*const CRTerm),
}

impl Terms {
    unsafe fn to_string(&self) -> String {
        match *self {
            Terms::Single(t) => from_glib_full(cr_term_one_to_string(t)),
            Terms::List(t) => from_glib_full(cr_term_to_string(t)),
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_color_from_term(
    term: *const CRTerm,
    out: *mut ClutterColor,
) -> GetFromTermResult {
    get_value_from_terms::<Color, ClutterColor>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_time_from_term(
    term: *const CRTerm,
    out: *mut f64,
) -> GetFromTermResult {
    get_value_from_terms::<Time, f64>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_double_from_term(
    term: *const CRTerm,
    out: *mut f64,
) -> GetFromTermResult {
    get_value_from_terms::<Number, f64>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_length_from_term(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut f64,
) -> GetFromTermResult {
    get_value_from_terms::<Length, Length>(&Terms::List(term))
        .map(|l| l.normalize(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_url_from_term(
    term: *const CRTerm,
    out: *mut *mut libc::c_char,
) -> GetFromTermResult {
    get_value_from_terms::<Url, Url>(&Terms::List(term))
        .map(|u| u.0.to_glib_full())
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_icon_style_from_term(
    term: *const CRTerm,
    out: *mut StIconStyle,
) -> GetFromTermResult {
    get_value_from_terms::<StIconStyle, StIconStyle>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_get_text_align_from_term(
    term: *const CRTerm,
    out: *mut StTextAlign,
) -> GetFromTermResult {
    get_value_from_terms::<StTextAlign, StTextAlign>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_background_position(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut StBackgroundPosition,
) -> GetFromTermResult {
    get_value_from_terms::<BackgroundPosition, BackgroundPosition>(&Terms::List(term))
        .map(|v| v.normalize_to_pixel_grid(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_background_size(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut StBackgroundSizeSpec,
) -> GetFromTermResult {
    get_value_from_terms::<BackgroundSize, BackgroundSize>(&Terms::List(term))
        .map(|v| v.normalize_to_pixel_grid(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_gradient_type(
    term: *const CRTerm,
    out: *mut StGradientType,
) -> GetFromTermResult {
    get_value_from_terms::<StGradientType, StGradientType>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_outline_shorthand(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut StOutline,
) -> GetFromTermResult {
    get_value_from_terms::<Outline, Outline>(&Terms::List(term))
        .map(|v| v.normalize_to_pixel_grid(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_shadow(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut StParsedShadow,
) -> GetFromTermResult {
    get_value_from_terms::<Shadow, Shadow>(&Terms::List(term))
        .map(|v| v.normalize(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_sides_shorthand(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut StSides,
) -> GetFromTermResult {
    get_value_from_terms::<Sides, Sides>(&Terms::List(term))
        .map(|v| v.normalize_to_pixel_grid(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_corners_shorthand(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut StCorners,
) -> GetFromTermResult {
    get_value_from_terms::<Corners, Corners>(&Terms::List(term))
        .map(|v| v.normalize_to_pixel_grid(&norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_text_decoration(
    term: *const CRTerm,
    out: *mut StTextDecoration,
) -> GetFromTermResult {
    get_value_from_terms::<TextDecoration, StTextDecoration>(&Terms::List(term)).to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_font_shorthand(
    term: *const CRTerm,
    norm: NormalizeParams,
    parent_weight: pango_sys::PangoWeight,
    out: *mut StFontSpec,
) -> GetFromTermResult {
    get_value_from_terms::<Font, Font>(&Terms::List(term))
        .map(|font| {
            let spec = font.to_font_spec();

            let style = pango::Style::from(spec.style).to_glib();
            let variant = pango::Variant::from(spec.variant).to_glib();
            let weight = spec.weight.compute_from_pango(parent_weight);
            let size = spec.size.compute(&norm).normalize(&(), &norm);
            let family = spec.family.as_str().to_glib_full();

            StFontSpec {
                style,
                variant,
                weight,
                size,
                family,
            }
        })
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_font_family(
    term: *const CRTerm,
    out: *mut *mut libc::c_char,
) -> GetFromTermResult {
    get_value_from_terms::<FontFamily, FontFamily>(&Terms::List(term))
        .map(|u| u.as_str().to_glib_full())
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_font_size(
    term: *const CRTerm,
    norm: NormalizeParams,
    out: *mut f64,
) -> GetFromTermResult {
    println!("norm.font_size: {}", norm.font_size);
    get_value_from_terms::<FontSize, FontSize>(&Terms::List(term))
        .map(|v| v.compute(&norm).normalize(&(), &norm))
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_font_style(
    term: *const CRTerm,
    out: *mut pango_sys::PangoStyle,
) -> GetFromTermResult {
    get_value_from_terms::<FontStyle, pango::Style>(&Terms::List(term))
        .map(|v| v.to_glib())
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_font_variant(
    term: *const CRTerm,
    out: *mut pango_sys::PangoVariant,
) -> GetFromTermResult {
    get_value_from_terms::<FontVariant, pango::Variant>(&Terms::List(term))
        .map(|v| v.to_glib())
        .to_shell(out)
}

#[no_mangle]
pub unsafe extern "C" fn stylish_parse_font_weight(
    term: *const CRTerm,
    parent_weight: pango_sys::PangoWeight,
    out: *mut pango_sys::PangoWeight,
) -> GetFromTermResult {
    get_value_from_terms::<FontWeight, FontWeight>(&Terms::List(term))
        .map(|w| w.compute_from_pango(parent_weight))
        .to_shell(out)
}

unsafe fn get_value_from_terms<In, Out>(terms: &Terms) -> ConvertedResult<Out>
where
    In: Clone + Default + Parse,
    Out: From<In>,
{
    let s = terms.to_string();
    get_converted_result_from_str(&s)
}

fn get_converted_result_from_str<In, Out>(s: &str) -> ConvertedResult<Out>
where
    In: Clone + Default + Parse,
    Out: From<In>,
{
    let mut input = ParserInput::new(s);
    let mut parser = Parser::new(&mut input);

    ConvertedResult::from::<In>(parse_input(&mut parser))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_simple_value() {
        assert_eq!(
            get_converted_result_from_str::<Number, f64>("5"),
            ConvertedResult::Found(5.0),
        );
    }

    #[test]
    fn parses_inherit() {
        assert_eq!(
            get_converted_result_from_str::<Number, f64>("inherit"),
            ConvertedResult::Inherit,
        );
    }

    #[test]
    fn parses_with_error() {
        assert_eq!(
            get_converted_result_from_str::<Number, f64>("foo"),
            ConvertedResult::NotFound
        );
    }

    #[test]
    fn parses_icon_style() {
        assert_eq!(
            get_converted_result_from_str::<StIconStyle, StIconStyle>("symbolic"),
            ConvertedResult::Found(StIconStyle::Symbolic),
        );
    }
}
