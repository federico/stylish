use cast::u16;
use cssparser::{self, Parser, Token};
use glib::translate::*;
use libc;
use pango;
use pango_sys;

use crate::error::*;
use crate::length::{Length, LengthUnit, NormalizeParams, POINTS_PER_INCH};
use crate::normalize::Normalize;
use crate::parsers::{finite_f32, Parse};
use crate::property_defs::{FontStretch, FontStyle, FontVariant, TextDecoration};

#[repr(transparent)]
pub struct StTextDecoration(pub libc::c_int);

#[rustfmt::skip]
mod constants {
    // Keep these in sync with StTextDecoration

    pub const ST_TEXT_DECORATION_UNDERLINE:    libc::c_int = 1 << 0;
    pub const ST_TEXT_DECORATION_OVERLINE:     libc::c_int = 1 << 1;
    pub const ST_TEXT_DECORATION_LINE_THROUGH: libc::c_int = 1 << 2;
    pub const ST_TEXT_DECORATION_BLINK:        libc::c_int = 1 << 3;
}

impl From<TextDecoration> for StTextDecoration {
    fn from(d: TextDecoration) -> StTextDecoration {
        use constants::*;

        let mut v = 0;

        if d.underline {
            v |= ST_TEXT_DECORATION_UNDERLINE;
        }

        if d.overline {
            v |= ST_TEXT_DECORATION_OVERLINE;
        }

        if d.line_through {
            v |= ST_TEXT_DECORATION_LINE_THROUGH;
        }

        if d.blink {
            v |= ST_TEXT_DECORATION_BLINK;
        }

        StTextDecoration(v)
    }
}

impl From<FontStyle> for pango::Style {
    fn from(s: FontStyle) -> pango::Style {
        match s {
            FontStyle::Normal => pango::Style::Normal,
            FontStyle::Italic => pango::Style::Italic,
            FontStyle::Oblique => pango::Style::Oblique,
        }
    }
}

impl From<FontVariant> for pango::Variant {
    fn from(v: FontVariant) -> pango::Variant {
        match v {
            FontVariant::Normal => pango::Variant::Normal,
            FontVariant::SmallCaps => pango::Variant::SmallCaps,
        }
    }
}

// https://www.w3.org/TR/CSS2/fonts.html#font-shorthand
// https://drafts.csswg.org/css-fonts-4/#font-prop
// servo/components/style/properties/shorthands/font.mako.rs is a good reference for this
#[derive(Debug, Clone, PartialEq)]
pub enum Font {
    Caption,
    Icon,
    Menu,
    MessageBox,
    SmallCaption,
    StatusBar,
    Spec(FontSpec),
}

#[derive(Debug, Default, Clone, PartialEq)]
pub struct FontSpec {
    pub style: FontStyle,
    pub variant: FontVariant,
    pub weight: FontWeight,
    pub stretch: FontStretch,
    pub size: FontSize,
    pub line_height: LineHeight,
    pub family: FontFamily,
}

impl Parse for Font {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Font, ParseError<'i>> {
        if let Ok(f) = parse_font_spec_identifiers(parser) {
            return Ok(f);
        }

        // The following is stolen from servo/components/style/properties/shorthands/font.mako.rs

        let mut nb_normals = 0;
        let mut style = None;
        let mut variant_caps = None;
        let mut weight = None;
        let mut stretch = None;
        let size;

        loop {
            // Special-case 'normal' because it is valid in each of
            // font-style, font-weight, font-variant and font-stretch.
            // Leaves the values to None, 'normal' is the initial value for each of them.
            if parser
                .try_parse(|input| input.expect_ident_matching("normal"))
                .is_ok()
            {
                nb_normals += 1;
                continue;
            }
            if style.is_none() {
                if let Ok(value) = parser.try_parse(FontStyle::parse) {
                    style = Some(value);
                    continue;
                }
            }
            if weight.is_none() {
                if let Ok(value) = parser.try_parse(FontWeight::parse) {
                    weight = Some(value);
                    continue;
                }
            }
            if variant_caps.is_none() {
                if let Ok(value) = parser.try_parse(FontVariant::parse) {
                    variant_caps = Some(value);
                    continue;
                }
            }
            if stretch.is_none() {
                if let Ok(value) = parser.try_parse(FontStretch::parse) {
                    stretch = Some(value);
                    continue;
                }
            }
            size = FontSize::parse(parser)?;
            break;
        }

        let line_height = if parser.try_parse(|input| input.expect_delim('/')).is_ok() {
            Some(LineHeight::parse(parser)?)
        } else {
            None
        };

        #[inline]
        fn count<T>(opt: &Option<T>) -> u8 {
            if opt.is_some() {
                1
            } else {
                0
            }
        }

        if (count(&style) + count(&weight) + count(&variant_caps) + count(&stretch) + nb_normals)
            > 4
        {
            return Err(parser.new_custom_error(ValueErrorKind::parse_error(
                "invalid syntax for 'font' property",
            )));
        }

        let family = FontFamily::parse(parser)?;

        Ok(Font::Spec(FontSpec {
            style: style.unwrap_or_default(),
            variant: variant_caps.unwrap_or_default(),
            weight: weight.unwrap_or_default(),
            stretch: stretch.unwrap_or_default(),
            size,
            line_height: line_height.unwrap_or_default(),
            family,
        }))
    }
}

impl Font {
    pub fn to_font_spec(&self) -> FontSpec {
        match *self {
            Font::Caption
            | Font::Icon
            | Font::Menu
            | Font::MessageBox
            | Font::SmallCaption
            | Font::StatusBar => {
                // We don't actually pick up the systme fonts, so reduce them to a default.
                FontSpec::default()
            }

            Font::Spec(ref spec) => spec.clone(),
        }
    }
}

#[rustfmt::skip]
fn parse_font_spec_identifiers<'i>(parser: &mut Parser<'i, '_>) -> Result<Font, ParseError<'i>> {
    Ok(parser.try_parse(|p| {
        parse_identifiers! {
            p,
            "caption"       => Font::Caption,
            "icon"          => Font::Icon,
            "menu"          => Font::Menu,
            "message-box"   => Font::MessageBox,
            "small-caption" => Font::SmallCaption,
            "status-bar"    => Font::StatusBar,
        }
    })?)
}

// Keep in sync with st-theme-node.c:StFontSpec
#[repr(C)]
pub struct StFontSpec {
    pub style: pango_sys::PangoStyle,
    pub variant: pango_sys::PangoVariant,
    pub weight: pango_sys::PangoWeight,
    pub size: f64,
    pub family: *mut libc::c_char,
}

/// https://www.w3.org/TR/2008/REC-CSS2-20080411/fonts.html#propdef-font-family
#[derive(Debug, Clone, PartialEq)]
pub struct FontFamily(pub String);

impl Parse for FontFamily {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<FontFamily, ParseError<'i>> {
        let loc = parser.current_source_location();

        let fonts = parser.parse_comma_separated(|parser| {
            if let Ok(cow) = parser.try_parse(|p| p.expect_string_cloned()) {
                if cow == "" {
                    return Err(loc.new_custom_error(ValueErrorKind::value_error(
                        "empty string is not a valid font family name",
                    )));
                }

                return Ok(cow);
            }

            let first_ident = parser.expect_ident()?.clone();
            let mut value = first_ident.as_ref().to_owned();

            while let Ok(cow) = parser.try_parse(|p| p.expect_ident_cloned()) {
                value.push(' ');
                value.push_str(&cow);
            }
            Ok(cssparser::CowRcStr::from(value))
        })?;

        Ok(FontFamily(fonts.join(",")))
    }
}

impl FontFamily {
    pub fn as_str(&self) -> &str {
        &self.0
    }
}

// https://www.w3.org/TR/2008/REC-CSS2-20080411/fonts.html#propdef-font-size
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum FontSize {
    Smaller,
    Larger,
    XXSmall,
    XSmall,
    Small,
    Medium,
    Large,
    XLarge,
    XXLarge,
    Value(Length),
}

impl FontSize {
    pub fn value(&self) -> Length {
        match self {
            FontSize::Value(s) => *s,
            _ => unreachable!(),
        }
    }

    pub fn compute(&self, v: &NormalizeParams) -> Self {
        let compute_points = |p| 12.0 * 1.2f64.powf(p) / POINTS_PER_INCH;

        // FIXME: this is analogous but deviates from librsvg, where v: ComputedValues
        let parent = Length::new(v.font_size, LengthUnit::Px);

        // The parent must already have resolved to an absolute unit
        assert!(
            parent.unit != LengthUnit::Percent
                && parent.unit != LengthUnit::Em
                && parent.unit != LengthUnit::Ex
        );

        use FontSize::*;

        #[rustfmt::skip]
        let new_size = match self {
            Smaller => Length::new(parent.length / 1.2,  parent.unit),
            Larger  => Length::new(parent.length * 1.2,  parent.unit),
            XXSmall => Length::new(compute_points(-3.0), LengthUnit::In),
            XSmall  => Length::new(compute_points(-2.0), LengthUnit::In),
            Small   => Length::new(compute_points(-1.0), LengthUnit::In),
            Medium  => Length::new(compute_points(0.0),  LengthUnit::In),
            Large   => Length::new(compute_points(1.0),  LengthUnit::In),
            XLarge  => Length::new(compute_points(2.0),  LengthUnit::In),
            XXLarge => Length::new(compute_points(3.0),  LengthUnit::In),

            Value(s) if s.unit == LengthUnit::Percent => {
                Length::new(parent.length * s.length, parent.unit)
            }

            Value(s) if s.unit == LengthUnit::Em => {
                Length::new(parent.length * s.length, parent.unit)
            }

            Value(s) if s.unit == LengthUnit::Ex => {
                // FIXME: it would be nice to know the actual Ex-height
                // of the font.
                Length::new(parent.length * s.length / 2.0, parent.unit)
            }

            Value(s) => *s,
        };

        FontSize::Value(new_size)
    }

    pub fn normalize(&self, _values: &(), params: &NormalizeParams) -> f64 {
        self.value().normalize(params)
    }
}

impl Parse for FontSize {
    #[rustfmt::skip]
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<FontSize, ParseError<'i>> {
        parser
            .try_parse(|p| Length::parse(p))
            .and_then(|l| Ok(FontSize::Value(l)))
            .or_else(|_| {
                Ok(parse_identifiers!(
                    parser,
                    "smaller"  => FontSize::Smaller,
                    "larger"   => FontSize::Larger,
                    "xx-small" => FontSize::XXSmall,
                    "x-small"  => FontSize::XSmall,
                    "small"    => FontSize::Small,
                    "medium"   => FontSize::Medium,
                    "large"    => FontSize::Large,
                    "x-large"  => FontSize::XLarge,
                    "xx-large" => FontSize::XXLarge,
                )?)
            })
    }
}

// https://drafts.csswg.org/css-fonts-4/#font-weight-prop
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum FontWeight {
    Normal,
    Bold,
    Bolder,
    Lighter,
    Weight(u16),
}

impl Parse for FontWeight {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<FontWeight, ParseError<'i>> {
        parser
            .try_parse(|p| {
                Ok(parse_identifiers!(
                    p,
                    "normal" => FontWeight::Normal,
                    "bold" => FontWeight::Bold,
                    "bolder" => FontWeight::Bolder,
                    "lighter" => FontWeight::Lighter,
                )?)
            })
            .or_else(|_: ParseError| {
                let loc = parser.current_source_location();
                let i = parser.expect_integer()?;
                if (1..=1000).contains(&i) {
                    Ok(FontWeight::Weight(u16(i).unwrap()))
                } else {
                    Err(loc.new_custom_error(ValueErrorKind::value_error(
                        "value must be between 1 and 1000 inclusive",
                    )))
                }
            })
    }
}

impl FontWeight {
    #[rustfmt::skip]
    pub fn compute(&self, v: &Self) -> Self {
        use FontWeight::*;

        // Here, note that we assume that Normal=W400 and Bold=W700, per the spec.  Also,
        // this must match `impl From<FontWeight> for pango::Weight`.
        //
        // See the table at https://drafts.csswg.org/css-fonts-4/#relative-weights

        match *self {
            Bolder => match v.numeric_weight() {
                w if (  1..100).contains(&w) => Weight(400),
                w if (100..350).contains(&w) => Weight(400),
                w if (350..550).contains(&w) => Weight(700),
                w if (550..750).contains(&w) => Weight(900),
                w if (750..900).contains(&w) => Weight(900),
                w if 900 <= w                => Weight(w),

                _ => unreachable!(),
            }

            Lighter => match v.numeric_weight() {
                w if (  1..100).contains(&w) => Weight(w),
                w if (100..350).contains(&w) => Weight(100),
                w if (350..550).contains(&w) => Weight(100),
                w if (550..750).contains(&w) => Weight(400),
                w if (750..900).contains(&w) => Weight(700),
                w if 900 <= w                => Weight(700),

                _ => unreachable!(),
            }

            _ => *self,
        }
    }

    // Converts the symbolic weights to numeric weights.  Will panic on `Bolder` or `Lighter`.
    pub fn numeric_weight(self) -> u16 {
        use FontWeight::*;

        // Here, note that we assume that Normal=W400 and Bold=W700, per the spec.  Also,
        // this must match `impl From<FontWeight> for pango::Weight`.
        match self {
            Normal => 400,
            Bold => 700,
            Bolder | Lighter => unreachable!(),
            Weight(w) => w,
        }
    }
}

impl FontWeight {
    // This will go away once we have ComputedValues to inherit from;
    // for now the best we can do is to obtain the parent_weight from gnome-shell
    // and do the computation as a special case.
    fn compute_from_weight(&self, parent_weight: u16) -> FontWeight {
        let parent_weight = FontWeight::Weight(parent_weight);
        self.compute(&parent_weight)
    }

    pub fn compute_from_pango(
        &self,
        parent_weight: pango_sys::PangoWeight,
    ) -> pango_sys::PangoWeight {
        // We don't convert parent_weight.from_glib() since that may give us
        // a non-numeric pango::Weight::Normal, for example.  It's easier to
        // deal with the explicit numeric weight here.
        //
        // First of all, we clamp to the allowed range for the property.
        let parent_weight = parent_weight.max(1000).min(1);
        let parent_weight = u16(parent_weight).unwrap();

        let pango_weight: pango::Weight = self.compute_from_weight(parent_weight).into();
        pango_weight.to_glib()
    }
}

impl From<FontWeight> for pango::Weight {
    fn from(w: FontWeight) -> pango::Weight {
        pango::Weight::__Unknown(w.numeric_weight().into())
    }
}

// https://www.w3.org/TR/CSS2/visudet.html#propdef-line-height
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum LineHeight {
    Normal,
    Number(f32),
    Length(Length),
    Percentage(f32),
}

impl Parse for LineHeight {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<LineHeight, ParseError<'i>> {
        let state = parser.state();
        let loc = parser.current_source_location();

        let token = parser.next()?.clone();

        match token {
            Token::Ident(ref cow) => {
                if cow.eq_ignore_ascii_case("normal") {
                    Ok(LineHeight::Normal)
                } else {
                    Err(parser.new_basic_unexpected_token_error(token.clone()))?
                }
            }

            Token::Number { value, .. } => Ok(LineHeight::Number(
                finite_f32(value).map_err(|e| loc.new_custom_error(e))?,
            )),

            Token::Percentage { unit_value, .. } => Ok(LineHeight::Percentage(unit_value)),

            Token::Dimension { .. } => {
                parser.reset(&state);
                Ok(LineHeight::Length(Length::parse(parser)?))
            }

            _ => Err(parser.new_basic_unexpected_token_error(token))?,
        }
    }
}
