use cssparser::{self, Parser};

use crate::error::ParseError;
use crate::parsers::Parse;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Url(pub String);

impl Parse for Url {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Url, ParseError<'i>> {
        let v = parser.expect_url_or_string()?;
        Ok(Url(v.as_ref().to_string()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_url() {
        assert_eq!(
            Url::parse_str("\"foo.svg\""),
            Ok(Url("foo.svg".to_string()))
        );
        assert_eq!(
            Url::parse_str("url(foo.svg)"),
            Ok(Url("foo.svg".to_string()))
        );
        assert_eq!(
            Url::parse_str("url(\"foo.svg\")"),
            Ok(Url("foo.svg".to_string()))
        );
        assert_eq!(
            Url::parse_str("url('foo.svg')"),
            Ok(Url("foo.svg".to_string()))
        );
    }

    #[test]
    fn invalid_url_yields_error() {
        assert!(Url::parse_str("").is_err());
        assert!(Url::parse_str("42").is_err());
    }
}
