use cssparser::{self, Parser};

use crate::error::ParseError;
use crate::parsers::Parse;

#[derive(Copy, Clone, Debug, Default, PartialEq)]
#[repr(C)]
pub struct Number(pub f64);

impl Parse for Number {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Number, ParseError<'i>> {
        Ok(Number(f64::from(parser.expect_number()?)))
    }
}

impl From<Number> for f64 {
    fn from(n: Number) -> f64 {
        n.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_number() {
        assert_eq!(Number::parse_str("42"), Ok(Number(42.0)));
    }

    #[test]
    fn invalid_number_yields_error() {
        assert!(Number::parse_str("").is_err());
        assert!(Number::parse_str("5em").is_err());
    }
}
