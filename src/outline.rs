use cssparser::{self, Parser};

use crate::color::{ClutterColor, Color};
use crate::error::ParseError;
use crate::length::{Length, NormalizeParams};
use crate::normalize::NormalizeToPixelGrid;
use crate::parsers::Parse;

// Keep this in sync with st-theme-node-private.h:StOutline
#[derive(Default)]
#[repr(C)]
pub struct StOutline {
    pub color: ClutterColor,
    pub width: i32,
}

#[derive(Default, Copy, Clone, PartialEq, Debug)]
pub struct Outline {
    pub color: Color,
    pub width: Length,
    pub style: OutlineStyle,
}

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum OutlineStyle {
    None,
    Solid,
    // Dotted, Dashed, etc. should go here but we don't support them yet
}

// This is the `outline` shorthand property
impl Parse for Outline {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Outline, ParseError<'i>> {
        let mut outline = Outline::default();

        loop {
            if let Ok(length) = parser.try_parse(|p| Length::parse(p)) {
                outline.width = length;
                if outline.width.length > 0.0 {
                    outline.style = OutlineStyle::Solid;
                }
                continue;
            }

            if let Ok(color) = parser.try_parse(|p| Color::parse(p)) {
                outline.color = color;
                continue;
            }

            if parser.is_exhausted() {
                break;
            }

            match parser.try_parse(|p| OutlineStyle::parse(p)) {
                Ok(style) => {
                    outline.style = style;
                    continue;
                }

                Err(e) => return Err(e),
            }
        }

        Ok(outline)
    }
}

impl Default for OutlineStyle {
    fn default() -> OutlineStyle {
        OutlineStyle::None
    }
}

impl Parse for OutlineStyle {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<OutlineStyle, ParseError<'i>> {
        let state = parser.state();

        match parser.expect_ident()?.as_ref() {
            "none" | "hidden" => Ok(OutlineStyle::None),

            "solid" => Ok(OutlineStyle::Solid),

            _ => {
                parser.reset(&state);
                Err(parser.new_error_for_next_token())
            }
        }
    }
}

impl NormalizeToPixelGrid for Outline {
    type Out = StOutline;

    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> StOutline {
        let width = match self.style {
            OutlineStyle::None => 0,

            OutlineStyle::Solid => self.width.normalize_to_pixel_grid(norm),
        };

        StOutline {
            color: ClutterColor::from(self.color),
            width,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::length::LengthUnit;

    #[test]
    fn parses_outline() {
        assert_eq!(
            Outline::parse_str("5px red"),
            Ok(Outline {
                color: Color::new(255, 0, 0, 255),
                width: Length::new(5.0, LengthUnit::Px),
                style: OutlineStyle::Solid,
            })
        );

        assert_eq!(
            Outline::parse_str("red 5px"),
            Ok(Outline {
                color: Color::new(255, 0, 0, 255),
                width: Length::new(5.0, LengthUnit::Px),
                style: OutlineStyle::Solid,
            })
        );

        assert_eq!(
            Outline::parse_str("red solid 5px"),
            Ok(Outline {
                color: Color::new(255, 0, 0, 255),
                width: Length::new(5.0, LengthUnit::Px),
                style: OutlineStyle::Solid,
            })
        );
    }

    #[test]
    fn invalid_outline_yields_error() {
        assert!(Outline::parse_str("foobar").is_err());
    }
}
