use cssparser::{self, Parser};

use crate::error::*;
use crate::parsers::Parse;

/// Representation of a single CSS property value.
///
/// `Unspecified` is the `Default`; it means that the corresponding property is not present.
///
/// `Inherit` means that the property is explicitly set to inherit
/// from the parent element.  This is useful for properties which the
/// SVG or CSS specs mandate that should not be inherited by default.
///
/// `Specified` is a value given by the SVG or CSS stylesheet.  This will later be
/// resolved into part of a `ComputedValues` struct.
#[derive(Clone)]
pub enum SpecifiedValue<T>
where
    //    T: Property<ComputedValues> + Clone + Default,
    T: Clone + Default,
{
    Unspecified,
    Inherit,
    Specified(T),
}

// Parses the value for the type `T` of the property out of the Parser, including `inherit` values.
pub fn parse_input<'i, T>(input: &mut Parser<'i, '_>) -> Result<SpecifiedValue<T>, ParseError<'i>>
where
    //    T: Property<ComputedValues> + Clone + Default + Parse,
    T: Clone + Default + Parse,
{
    if input
        .try_parse(|p| p.expect_ident_matching("inherit"))
        .is_ok()
    {
        Ok(SpecifiedValue::Inherit)
    } else {
        Parse::parse(input).map(SpecifiedValue::Specified)
    }
}
