use cssparser::{self, Parser};

use crate::error::ParseError;
use crate::length::{Length, LengthOrAuto, NormalizeParams};
use crate::normalize::NormalizeToPixelGrid;
use crate::parsers::Parse;

#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(C)]
pub struct StBackgroundPosition {
    x: i32,
    y: i32,
}

#[derive(Default, Copy, Clone, PartialEq, Debug)]
pub struct BackgroundPosition {
    x: Length,
    y: Length,
}

impl Parse for BackgroundPosition {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<BackgroundPosition, ParseError<'i>> {
        let x = Length::parse(parser)?;
        let y = Length::parse(parser)?;
        Ok(BackgroundPosition { x, y })
    }
}

impl NormalizeToPixelGrid for BackgroundPosition {
    type Out = StBackgroundPosition;

    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> StBackgroundPosition {
        StBackgroundPosition {
            x: self.x.normalize_to_pixel_grid(norm),
            y: self.y.normalize_to_pixel_grid(norm),
        }
    }
}

// Keep this in sync with st-theme-node-private.h:StBackgroundSize
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(C)]
pub enum StBackgroundSize {
    Auto,
    Contain,
    Cover,
    Fixed,
}

#[repr(C)]
pub struct StBackgroundSizeSpec {
    kind: StBackgroundSize,
    width: i32,
    height: i32,
}

impl StBackgroundSizeSpec {
    fn from_kind(kind: StBackgroundSize) -> StBackgroundSizeSpec {
        StBackgroundSizeSpec {
            kind,
            width: -1,
            height: -1,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum BackgroundSize {
    Explicit {
        width: LengthOrAuto,
        height: LengthOrAuto,
    },

    Cover,
    Contain,
}

impl Parse for BackgroundSize {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<BackgroundSize, ParseError<'i>> {
        if let Ok(width) = parser.try_parse(|p| LengthOrAuto::parse(p)) {
            let height = parser
                .try_parse(|p| LengthOrAuto::parse(p))
                .unwrap_or(LengthOrAuto::Auto);

            Ok(BackgroundSize::Explicit { width, height })
        } else {
            Ok(parse_identifiers! {
                parser,
                "cover" => BackgroundSize::Cover,
                "contain" => BackgroundSize::Contain,
            }?)
        }
    }
}

impl NormalizeToPixelGrid for BackgroundSize {
    type Out = StBackgroundSizeSpec;

    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> StBackgroundSizeSpec {
        match *self {
            BackgroundSize::Cover => StBackgroundSizeSpec::from_kind(StBackgroundSize::Cover),
            BackgroundSize::Contain => StBackgroundSizeSpec::from_kind(StBackgroundSize::Contain),

            BackgroundSize::Explicit { width: LengthOrAuto::Auto, height: LengthOrAuto::Auto } =>
                StBackgroundSizeSpec::from_kind (StBackgroundSize::Auto),

            BackgroundSize::Explicit { width, height } => StBackgroundSizeSpec {
                kind: StBackgroundSize::Fixed,
                width: width.normalize_to_pixel_grid(norm),
                height: height.normalize_to_pixel_grid(norm),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::length::LengthUnit;

    use super::*;

    #[test]
    fn parses_background_size() {
        assert_eq!(
            BackgroundSize::parse_str("auto").unwrap(),
            BackgroundSize::Explicit {
                width: LengthOrAuto::Auto,
                height: LengthOrAuto::Auto,
            }
        );

        assert_eq!(
            BackgroundSize::parse_str("4px").unwrap(),
            BackgroundSize::Explicit {
                width: LengthOrAuto::Length(Length::new(4.0, LengthUnit::Px)),
                height: LengthOrAuto::Auto,
            }
        );
        assert_eq!(
            BackgroundSize::parse_str("auto auto").unwrap(),
            BackgroundSize::Explicit {
                width: LengthOrAuto::Auto,
                height: LengthOrAuto::Auto,
            }
        );

        assert_eq!(
            BackgroundSize::parse_str("auto 4px").unwrap(),
            BackgroundSize::Explicit {
                width: LengthOrAuto::Auto,
                height: LengthOrAuto::Length(Length::new(4.0, LengthUnit::Px)),
            }
        );

        assert_eq!(
            BackgroundSize::parse_str("4px auto").unwrap(),
            BackgroundSize::Explicit {
                width: LengthOrAuto::Length(Length::new(4.0, LengthUnit::Px)),
                height: LengthOrAuto::Auto,
            }
        );

        assert_eq!(
            BackgroundSize::parse_str("cover").unwrap(),
            BackgroundSize::Cover,
        );

        assert_eq!(
            BackgroundSize::parse_str("contain").unwrap(),
            BackgroundSize::Contain,
        );
        
    }
}
