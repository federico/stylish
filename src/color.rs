use cssparser::{self, Parser, RGBA};

use crate::error::ParseError;
use crate::parsers::Parse;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Color(pub RGBA);

impl Color {
    pub fn new(red: u8, green: u8, blue: u8, alpha: u8) -> Color {
        Color(RGBA {
            red,
            green,
            blue,
            alpha,
        })
    }

    pub fn black() -> Color {
        Color(RGBA {
            red: 0,
            green: 0,
            blue: 0,
            alpha: 0xff,
        })
    }
}

impl Default for Color {
    fn default() -> Color {
        Color(RGBA {
            red: 0,
            green: 0,
            blue: 0,
            alpha: 0,
        })
    }
}

impl Parse for Color {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Color, ParseError<'i>> {
        let state = parser.state();

        match cssparser::Color::parse(parser)? {
            cssparser::Color::RGBA(rgba) => Ok(Color(rgba)),

            cssparser::Color::CurrentColor => {
                // We don't support currentColor in gnome-shell
                // The token is already parsed, so reset the parser state and synthesize
                // an error at the original position

                parser.reset(&state);
                Err(parser.new_error_for_next_token())
            }
        }
    }
}

// Keep this in sync with clutter-color.h:ClutterColor
#[derive(Clone, Copy, Default, PartialEq, Debug)]
#[repr(C)]
pub struct ClutterColor {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    pub alpha: u8,
}

impl From<Color> for ClutterColor {
    fn from(color: Color) -> ClutterColor {
        ClutterColor {
            red: color.0.red,
            green: color.0.green,
            blue: color.0.blue,
            alpha: color.0.alpha,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use cssparser::{BasicParseErrorKind, ParseErrorKind, Token};

    #[test]
    fn parses_transparent() {
        assert_eq!(Color::parse_str("transparent"), Ok(Color::new(0, 0, 0, 0)));
    }

    #[test]
    fn parses_colors() {
        assert_eq!(Color::parse_str("red"), Ok(Color::new(255, 0, 0, 255)));
        assert_eq!(
            Color::parse_str("#00ff00aa"),
            Ok(Color::new(0, 255, 0, 0xaa))
        );
        assert_eq!(
            Color::parse_str("rgb(128, 128, 0, 50%)"),
            Ok(Color::new(128, 128, 0, 128))
        );
        assert_eq!(
            Color::parse_str("hsl(0, 100%, 50%)"),
            Ok(Color::new(255, 0, 0, 255))
        );
    }

    #[test]
    fn current_color_is_error() {
        let e = Color::parse_str("currentColor").expect_err("should be an error");
        match e.kind {
            ParseErrorKind::Basic(BasicParseErrorKind::UnexpectedToken(Token::Ident(ref t))) => {
                assert!(*t == "currentColor")
            }
            _ => unreachable!(),
        }
    }
}
