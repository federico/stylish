use cssparser::{self, Parser};
use glib::translate::*;
use glib_sys::gboolean;

use crate::color::{ClutterColor, Color};
use crate::error::ParseError;
use crate::length::{Length, NormalizeParams};
use crate::normalize::Normalize;
use crate::parsers::Parse;

// Keep this in sync with st-theme-node-private.h:StParsedShadow
#[derive(Default)]
#[repr(C)]
pub struct StParsedShadow {
    pub is_none: gboolean,
    pub color: ClutterColor,
    pub xoffset: f64,
    pub yoffset: f64,
    pub blur: f64,
    pub spread: f64,
    pub inset: gboolean,
}

/// https://www.w3.org/TR/css-backgrounds-3/#propdef-box-shadow
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Shadow {
    None,
    Shadow {
        color: Color,
        xoffset: Length,
        yoffset: Length,
        blur: Length,
        spread: Length,
        inset: bool,
    },
}

impl Default for Shadow {
    fn default() -> Shadow {
        Shadow::None
    }
}

impl Parse for Shadow {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Shadow, ParseError<'i>> {
        let state = parser.state();

        if parser
            .try_parse(|p| p.expect_ident_matching("none"))
            .is_ok()
        {
            parser.expect_exhausted()?;

            Ok(Shadow::None)
        } else {
            let inset = parser
                .try_parse(|p| p.expect_ident_matching("inset"))
                .is_ok();

            // The following two are mandatory
            let xoffset = Length::parse(parser)?;
            let yoffset = Length::parse(parser)?;

            // The following three are optional; we resolve them to defaults if missing

            let blur = parser
                .try_parse(|p| Length::parse(p))
                .unwrap_or_else(|_| Length::default());
            let spread = parser
                .try_parse(|p| Length::parse(p))
                .unwrap_or_else(|_| Length::default());

            if blur.length < 0.0 || spread.length < 0.0 {
                parser.reset(&state);
                // FIXME: use value-specific errors like in librsvg to say that
                // a negative value is not allowed here.
                return Err(parser.new_error_for_next_token());
            }

            // FIXME: https://www.w3.org/TR/css-backgrounds-3/#propdef-box-shadow says that
            // if the color is missing, then it gets resolved to the "color" property.
            // Does gnome-shell have that?
            //
            // For now we resolve to black, per gnome-shell's original code.
            let color = parser
                .try_parse(|p| Color::parse(p))
                .unwrap_or_else(|_| Color::black());

            parser.expect_exhausted()?;

            Ok(Shadow::Shadow {
                color,
                xoffset,
                yoffset,
                blur,
                spread,
                inset,
            })
        }
    }
}

impl Normalize for Shadow {
    type Out = StParsedShadow;

    fn normalize(&self, norm: &NormalizeParams) -> StParsedShadow {
        match *self {
            Shadow::None => StParsedShadow {
                is_none: true.to_glib(),
                color: Default::default(),
                xoffset: 0.0,
                yoffset: 0.0,
                blur: 0.0,
                spread: 0.0,
                inset: false.to_glib(),
            },

            Shadow::Shadow {
                color,
                xoffset,
                yoffset,
                blur,
                spread,
                inset,
                ..
            } => StParsedShadow {
                is_none: false.to_glib(),
                color: color.into(),
                xoffset: xoffset.normalize(&norm),
                yoffset: yoffset.normalize(&norm),
                blur: blur.normalize(&norm),
                spread: spread.normalize(&norm),
                inset: inset.to_glib(),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::length::LengthUnit;

    #[test]
    fn parses_shadow_none() {
        assert_eq!(Shadow::parse_str("none"), Ok(Shadow::None),);
    }

    #[test]
    fn parses_shadow_inset() {
        assert_eq!(
            Shadow::parse_str("inset 1 2"),
            Ok(Shadow::Shadow {
                inset: true,
                color: Color::black(),
                xoffset: Length::new(1.0, LengthUnit::Px),
                yoffset: Length::new(2.0, LengthUnit::Px),
                blur: Length::default(),
                spread: Length::default(),
            })
        );

        assert_eq!(
            Shadow::parse_str("1 2"),
            Ok(Shadow::Shadow {
                inset: false,
                color: Color::black(),
                xoffset: Length::new(1.0, LengthUnit::Px),
                yoffset: Length::new(2.0, LengthUnit::Px),
                blur: Length::default(),
                spread: Length::default(),
            })
        );
    }

    #[test]
    fn parses_shadow_args() {
        assert_eq!(
            Shadow::parse_str("inset 1 2 3 red"),
            Ok(Shadow::Shadow {
                inset: true,
                color: Color::new(0xff, 0, 0, 0xff),
                xoffset: Length::new(1.0, LengthUnit::Px),
                yoffset: Length::new(2.0, LengthUnit::Px),
                blur: Length::new(3.0, LengthUnit::Px),
                spread: Length::default(),
            })
        );

        assert_eq!(
            Shadow::parse_str("inset 1 2 3 4 red"),
            Ok(Shadow::Shadow {
                inset: true,
                color: Color::new(0xff, 0, 0, 0xff),
                xoffset: Length::new(1.0, LengthUnit::Px),
                yoffset: Length::new(2.0, LengthUnit::Px),
                blur: Length::new(3.0, LengthUnit::Px),
                spread: Length::new(4.0, LengthUnit::Px),
            })
        );
    }

    #[test]
    fn invalid_shadow_yields_error() {
        assert!(Shadow::parse_str("").is_err());
        assert!(Shadow::parse_str("none inset").is_err());
        assert!(Shadow::parse_str("none 1 2").is_err());
        assert!(Shadow::parse_str("1").is_err());
        assert!(Shadow::parse_str("1 red").is_err());
        assert!(Shadow::parse_str("1 2 -3 4 red").is_err());
        assert!(Shadow::parse_str("1 2 3 -4 red").is_err());
        assert!(Shadow::parse_str("inset 1 2 3 4 5 red").is_err());
        assert!(Shadow::parse_str("inset 1 2 3 4 red blue").is_err());
    }
}
