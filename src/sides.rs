use cssparser::{self, Parser};

use crate::error::ParseError;
use crate::length::{Length, NormalizeParams};
use crate::normalize::NormalizeToPixelGrid;
use crate::parsers::Parse;

// Keep this in sync with st-theme-node-private.h:StSides
#[derive(Default)]
#[repr(C)]
pub struct StSides {
    pub top: i32,
    pub right: i32,
    pub bottom: i32,
    pub left: i32,
}

#[derive(Default, Copy, Clone, PartialEq, Debug)]
pub struct Sides {
    pub top: Length,
    pub right: Length,
    pub bottom: Length,
    pub left: Length,
}

impl Parse for Sides {
    fn parse<'i>(parser: &mut Parser<'i, '_>) -> Result<Sides, ParseError<'i>> {
        let mut l = vec![];

        for _ in 0..4 {
            l.push(Length::parse(parser)?);

            if parser.is_exhausted() {
                break;
            }
        }

        #[rustfmt::skip]
        let sides = match l.len() {
            1 => Sides {
                top:    l[0],
                right:  l[0],
                bottom: l[0],
                left:   l[0],
            },

            2 => Sides {
                top:    l[0],
                bottom: l[0],
                left:   l[1],
                right:  l[1],
            },

            3 => Sides {
                top:    l[0],
                left:   l[1],
                right:  l[1],
                bottom: l[2],
            },

            4 => Sides {
                top:    l[0],
                right:  l[1],
                bottom: l[2],
                left:   l[3],
            },

            _ => unreachable!(),
        };

        Ok(sides)
    }
}

impl NormalizeToPixelGrid for Sides {
    type Out = StSides;

    fn normalize_to_pixel_grid(&self, norm: &NormalizeParams) -> StSides {
        let Sides {
            top,
            right,
            bottom,
            left,
        } = *self;

        StSides {
            top: top.normalize_to_pixel_grid(&norm),
            right: right.normalize_to_pixel_grid(&norm),
            bottom: bottom.normalize_to_pixel_grid(&norm),
            left: left.normalize_to_pixel_grid(&norm),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::length::LengthUnit;

    #[rustfmt::skip]
    #[test]
    fn parses_sides() {
        assert_eq!(
            Sides::parse_str("1px"),
            Ok(Sides {
                top:    Length::new(1.0, LengthUnit::Px),
                right:  Length::new(1.0, LengthUnit::Px),
                left:   Length::new(1.0, LengthUnit::Px),
                bottom: Length::new(1.0, LengthUnit::Px),
            })
        );

        assert_eq!(
            Sides::parse_str("1px 2px"),
            Ok(Sides {
                top:    Length::new(1.0, LengthUnit::Px),
                bottom: Length::new(1.0, LengthUnit::Px),
                left:   Length::new(2.0, LengthUnit::Px),
                right:  Length::new(2.0, LengthUnit::Px),
            })
        );

        assert_eq!(
            Sides::parse_str("1px 2px 3px"),
            Ok(Sides {
                top:    Length::new(1.0, LengthUnit::Px),
                left:   Length::new(2.0, LengthUnit::Px),
                right:  Length::new(2.0, LengthUnit::Px),
                bottom: Length::new(3.0, LengthUnit::Px),
            })
        );

        assert_eq!(
            Sides::parse_str("1px 2px 3px 4px"),
            Ok(Sides {
                top:    Length::new(1.0, LengthUnit::Px),
                right:  Length::new(2.0, LengthUnit::Px),
                bottom: Length::new(3.0, LengthUnit::Px),
                left:   Length::new(4.0, LengthUnit::Px),
            })
        );
    }
}
