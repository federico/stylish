# Stylish - styles for gnome-shell

This module intends to provide the CSS styling machinery for
gnome-shell.

We are gradually moving gnome-shell from using libcroco and its own
C code for CSS matching, to using the Rust crates that Mozilla Firefox
and Servo use for CSS parsing and matching.

# Changes from gnome-shell's parsers

## Colors

Supports the following:

* `#rgb`
* `#rgba`
* `#rrggbb`
* `#rrggbbaa`
* `rgb(r, g, b)` - numbers and percentages
* `rgb(r, g, b, a)` - numbers and percentages
* `rgba(x, y, z, a)`
* `hsl(h, s%, l%)`
* `hsl(45deg, s%, l%)`
* `hsl(h, s, l, a)`
* `hsla(h, s, l, a)`
* The seven above, with or without commas

## Outline and border

The `outline`, `border`, and
`border-top`/`border-right`/`border-bottom`/`border-left` shorthand
properties can have the following styles:

* `none`
* `hidden` (equivalent to `none`)
* `solid`

The old code handled the following but ignored them; the new code
considers them errors:

* `dotted`
* `dashed`
* `double`
* `groove`
* `ridge`
* `inset`
* `outset`

## Shadow

Unlike CSS3, gnome-shell allowed `shadow`-typed properties to have the
`inset` keyword and color value in any position, intermixed with the
xoffset/yoffset/blur/spread values.  Stylish requires strict
[CSS3-like
ordering](https://www.w3.org/TR/css-backgrounds-3/#propdef-box-shadow):

```
   <shadow> = inset? && <length>{2,4} && <color>?
```

However, while in CSS3 an omitted `<color>` means to use the element's
`color` property, gnome-shell and Stylish have no such concept.  If
the `<color>` is omitted, it will resolve to opaque black.
